# axreng-test

A Web crawler is a program that traverses the web by following the ever changing, dense and distributed hyperlinked structure and thereafter storing downloaded pages in a large repository which is later indexed for efficient execution of user queries [1]

# Main features:
1. Search keywork
2. Get status

# Requirements
1. Search a keyword on the web pages crawled. The keyword lenght must be between 4 and 32 and case-insensitive.
2. Response code must be a 8 lenght code no repetead
3. The process must be async. Return the code and keep the process on background.
4. The response to request must be: 200 OK, application/json, {"id":"???"}
5. The user want configure the seed (startup URL) using BASE_URL enviroment variable
6. The crawler must navigate only subdomain's BASE_URL
7. The seed can use HTTP or HTTPS protocol
8. The user can set the number of results found using MAX_RESULTS enviroment variable

# Used patters
1. Sigleton
2. Strategy
3. DAO


# Ref
1. <https://ieeexplore.ieee.org/document/7148493>