package br.com.ericmoraess.requests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

public class MakeRequestTest {

	@Test
	public void generateRandomKeys() {
		int length = 8;
		boolean useLetters = true;
		boolean useNumbers = true;
		String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
		assertEquals(generatedString.length(), 8);
	}
}
