package br.com.ericmoraess.exception;

public class BaseURLNotDefinedException extends Exception {
	private static final long serialVersionUID = -1652689415783220044L;

	public BaseURLNotDefinedException(String msg) {
		super(msg);
	}
}
