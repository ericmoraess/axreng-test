package br.com.ericmoraess.exception;

public class BaseURLInvalidException extends Exception {
	private static final long serialVersionUID = -7414450642830884699L;

	public BaseURLInvalidException(String msg) {
		super(msg);
	}
}
