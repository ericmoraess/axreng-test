package br.com.ericmoraess.persistence;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import com.br.ericmoraess.core.ICrawler;

public final class InMemoryDAOImp implements IDAO<ICrawler> {
	
	private static ConcurrentHashMap<String, ICrawler> repository;
	private static InMemoryDAOImp self;
	
	private InMemoryDAOImp() {}
	
	public static InMemoryDAOImp getInstance() {
		if(self == null) {
			repository = new ConcurrentHashMap<String, ICrawler>();
			self = new InMemoryDAOImp();
		}
		return self;
	}

	@Override
	public Optional<ICrawler> get(String id) {
		return Optional.ofNullable(repository.get(id));
	}

	@Override
	public List<ICrawler> getAll() {
		return null;
	}

	@Override
	public void save(ICrawler t) {
		repository.put(t.getResult().getId(), t);
	}

	@Override
	public void update(ICrawler t, String[] params) {
		// TODO Auto-generated method stub
	}

	@Override
	public void delete(ICrawler t) {
		// TODO Auto-generated method stub
	}
}
