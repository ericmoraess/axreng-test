package br.com.ericmoraess.config;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

public class ConfigurationFile {
	private static Properties properties;
	private static ConfigurationFile self;
	
	public static ConfigurationFile getInstance(String fileName) throws IOException {
		if(self == null) {
			self = new ConfigurationFile(fileName);
		}
		return self;
	}
	
	private ConfigurationFile(String fileName) throws IOException {
		properties = new Properties();
		var inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
		properties.load(inputStream);
	}
	
	public Optional<String> getProperty(String key) {
		return Optional.ofNullable(properties.getProperty(key));
	}
}
