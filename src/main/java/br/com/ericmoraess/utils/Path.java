package br.com.ericmoraess.utils;

public class Path {
	public static class Web {
		//	crawl routes
		public static String GET_CRAWL = "/crawl/:id";
		public static String DO_CRAWL  = "/crawl";
		public static String JSON_TYPE = "application/json";
		
	}
}
