package br.com.ericmoraess.utils;

import java.net.MalformedURLException;
import java.net.URL;

public class URLHelper {
	
	public static String getHost(String url) throws MalformedURLException {
		return new URL(url).getHost();
	}
	
}
