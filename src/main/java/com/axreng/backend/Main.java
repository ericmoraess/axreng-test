package com.axreng.backend;

import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.post;

import java.io.IOException;
import java.net.URL;

import com.br.ericmoraess.controller.CrawlerController;
import com.br.ericmoraess.core.ConfigCrawler;
import com.br.ericmoraess.entity.RequestDTO;

import br.com.ericmoraess.config.ConfigurationFile;
import br.com.ericmoraess.config.Messages;
import br.com.ericmoraess.exception.BaseURLInvalidException;
import br.com.ericmoraess.exception.BaseURLNotDefinedException;
import br.com.ericmoraess.persistence.InMemoryDAOImp;
import br.com.ericmoraess.utils.Path;

public class Main {
	
    public static void main(String[] args) throws IOException, BaseURLNotDefinedException, BaseURLInvalidException {
    	
    	var props = ConfigurationFile.getInstance("prod.properties");
    	var minSizeKeyword = Integer.valueOf(props.getProperty("crawler.keyword.minsize").get());
    	var maxSizeKeyword = Integer.valueOf(props.getProperty("crawler.keyword.maxsize").get());
		var configCrawler = getConfiguration(props);  
    	
		before((request, response) -> response.type("application/json"));
		
		before(Path.Web.GET_CRAWL, (req, res) -> {
			var id = req.params("id");
			var crawlerID = InMemoryDAOImp.getInstance().get(id);
        	if(crawlerID.isEmpty()) {
	        		halt(401, Messages.getString("exception.invalidID") );
        	}
        });
		
        get(Path.Web.GET_CRAWL, (req, res) -> {
        	return CrawlerController.handleStatusSearch(req, res);
        });
        
        before(Path.Web.DO_CRAWL, (req, res) -> {
        	var body = req.body();
        	if(!"".equals(body)) {
	        	var requestDTO = (RequestDTO)new RequestDTO().parser(req.body(), RequestDTO.class);
	        	int sizeKeyword = 0;
	        	try {
		        	sizeKeyword = requestDTO.getKeyword().length();
	        	}catch (Exception e) {
	        		halt(401, Messages.getString("exception.invalidFormatRequest"));
	        	}
	        	
	        	if (sizeKeyword < minSizeKeyword || sizeKeyword > maxSizeKeyword) {
	        		halt(401, Messages.getString("exception.youAreNotWelcome") + " " + minSizeKeyword  + " and " + maxSizeKeyword + ". Size keyword: " + sizeKeyword);
	        	}
        	}
        });
        
        post(Path.Web.DO_CRAWL, Path.Web.JSON_TYPE, (req, res) -> {
        	return CrawlerController.handleNewSearch(configCrawler, req, res);
        });
    }

	private static ConfigCrawler getConfiguration(ConfigurationFile props) throws BaseURLNotDefinedException, BaseURLInvalidException {
		var configCrawler = new ConfigCrawler();
		var baseURLVariableName = props.getProperty("crawler.baseURLEnvName"); 
		var baseURL = System.getenv(baseURLVariableName.get());
		
		if(baseURL == null) {
			throw new BaseURLNotDefinedException(Messages.getString("exception.youMustDefineBaseUrl"));
		}
				
		try {
			new URL(baseURL).toURI();
		}catch (Exception e) {
			throw new BaseURLInvalidException(Messages.getString("exception.youMustDefineValidUrl"));
		}
		
		configCrawler.setBaseURL(baseURL);
				
		var maxResultEnvVariableName = props.getProperty("crawler.maxResultEnvName");
		if(maxResultEnvVariableName.isPresent()) {
			var max = System.getenv(maxResultEnvVariableName.get());
			if( max != null) {
				var maxResult = Integer.valueOf(max);
				configCrawler.setMaxURLVisit(maxResult);
			}
		}
		
		return configCrawler;
	}
}
