package com.br.ericmoraess.core;

import java.util.concurrent.Callable;

public interface ICrawlerStrategy {
	public void execute(Callable<ICrawler> crawler);
}
