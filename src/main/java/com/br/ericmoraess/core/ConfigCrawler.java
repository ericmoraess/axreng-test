package com.br.ericmoraess.core;

public class ConfigCrawler {
	private final int DEFAULT_VALUE_NO_LIMITS = -1;
	private int maxURLVisit = Integer.MAX_VALUE;
	private String baseURL;
	private String keyword;
	
	public ConfigCrawler() {}

	public int getMaxURLVisit() {
		return maxURLVisit;
	}

	public void setMaxURLVisit(int maxURLVisit) {
		if(maxURLVisit > DEFAULT_VALUE_NO_LIMITS) {
			this.maxURLVisit = maxURLVisit;
		}
	}

	public String getBaseURL() {
		return baseURL;
	}

	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
}
