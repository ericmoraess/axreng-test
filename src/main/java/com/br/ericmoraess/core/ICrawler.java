package com.br.ericmoraess.core;

import java.util.concurrent.Callable;

import com.br.ericmoraess.entity.IResult;

public interface ICrawler extends Callable<ICrawler>{
		IResult getResult();
}
