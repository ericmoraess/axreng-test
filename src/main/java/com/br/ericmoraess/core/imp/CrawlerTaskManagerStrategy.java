package com.br.ericmoraess.core.imp;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.br.ericmoraess.core.ICrawler;
import com.br.ericmoraess.core.ICrawlerStrategy;

import br.com.ericmoraess.config.ConfigurationFile;

public final class CrawlerTaskManagerStrategy implements ICrawlerStrategy {
	private static final int corePoolSizeDefault = 4;
	private final ExecutorService executorService; 
	private static CrawlerTaskManagerStrategy self;
	
	private CrawlerTaskManagerStrategy(int corePoolSize) {
		var corePool = corePoolSize > 0 ? corePoolSize : corePoolSizeDefault;
		executorService = Executors.newCachedThreadPool();
	}

	public static CrawlerTaskManagerStrategy getInstance() {
		int corePool = 0;
		try {
			var corePoolSize = ConfigurationFile.getInstance("prod.properties").getProperty("crawler.taskman.numthread.pool");
			if(corePoolSize.isPresent()) {
				corePool = Integer.valueOf(corePoolSize.get());
			}
		} catch (IOException e) {}
		
		return getInstance(corePool);
	}
	
	public static CrawlerTaskManagerStrategy getInstance(int corePoolSize) {
		if (self == null) {
			self = new CrawlerTaskManagerStrategy(corePoolSize);
		}
		return self;
	}

	@Override
	public void execute(Callable<ICrawler> crawler) {
		executorService.submit(crawler);
	}
}
