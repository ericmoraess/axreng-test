package com.br.ericmoraess.core.imp;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.br.ericmoraess.common.StatusCrawler;
import com.br.ericmoraess.core.ConfigCrawler;
import com.br.ericmoraess.core.ICrawler;
import com.br.ericmoraess.entity.IResult;
import com.br.ericmoraess.entity.ResponseDTO;

import br.com.ericmoraess.persistence.IDAO;
import br.com.ericmoraess.utils.URLHelper;

public class RecursiveWebCrawler implements ICrawler {
	private IResult result;
	private IDAO<ICrawler> repository;
	private ConfigCrawler config;
		
	private List<String> urlsFounds;
	private Set<String> bufferURLs;
	
	public RecursiveWebCrawler(ConfigCrawler config, IDAO<ICrawler> repository) {		
		this.repository = repository;
		this.config = config;
		
		urlsFounds = new ArrayList<String>();
		bufferURLs = new HashSet<String>();
		
		result = new ResponseDTO(StatusCrawler.ACTIVE.toString().toLowerCase(), urlsFounds);
	}
	
	@Override
	public ICrawler call() throws Exception {
		repository.save(this);
		getPageLinks(config.getBaseURL());
		result.setStatus(StatusCrawler.DONE.toString().toLowerCase());
		return this;
	}

	@Override
	public IResult getResult() {
		return result;
	}
	
	public void getPageLinks(String URL) throws MalformedURLException {
		if( !config.getBaseURL().contains( URLHelper.getHost(URL) ) || urlsFounds.size() >= config.getMaxURLVisit() )
			return;
		
        if (!bufferURLs.contains(URL)) {
            try {

            	bufferURLs.add(URL);
                
                Document document = Jsoup.connect(URL).get();
                
                var found = StringUtils.indexOfIgnoreCase(document.toString(), config.getKeyword());
                
                if(found != -1) {
                	urlsFounds.add(URL);
                }
                
                Elements linksOnPage = document.select("a[href]");
                
                for (Element page : linksOnPage) {
                    getPageLinks(page.attr("abs:href"));
                }
                
            } catch (IOException e) {
                System.err.println("For '" + URL + "': " + e.getMessage());
            }
        }
    }
}
