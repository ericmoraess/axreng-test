package com.br.ericmoraess.controller;

import com.br.ericmoraess.core.ConfigCrawler;
import com.br.ericmoraess.core.ICrawler;
import com.br.ericmoraess.core.imp.CrawlerTaskManagerStrategy;
import com.br.ericmoraess.core.imp.RecursiveWebCrawler;
import com.br.ericmoraess.entity.RequestDTO;
import com.br.ericmoraess.entity.ResquestResponseDTO;

import br.com.ericmoraess.persistence.IDAO;
import br.com.ericmoraess.persistence.InMemoryDAOImp;
import spark.Request;
import spark.Response;

public class CrawlerController {
	
	private static IDAO<ICrawler> repository = InMemoryDAOImp.getInstance();

	public static String handleNewSearch(ConfigCrawler configCrawler, Request req, Response res) {
		
		var requestDTO = (RequestDTO) new RequestDTO().parser(req.body(), RequestDTO.class);
		
		/* we could use a cache mechanism to avoid crawl the same keyword many times.
		 * But, a page can change in another moment...
		 * I choose crawl always the user request even using the same keyword
		 * Because there is not requirement to use cache.
		 */
		
		configCrawler.setKeyword(requestDTO.getKeyword());
		var crawler = new RecursiveWebCrawler(configCrawler, repository);
	    CrawlerTaskManagerStrategy.getInstance().execute(crawler);
		
	    var response = new ResquestResponseDTO(crawler.getResult().getId()).toJson();
	    
		return response;
	}

	public static Object handleStatusSearch(Request req, Response res) {
		var id = req.params("id");
		var crawler = repository.get(id);
		return crawler.get().getResult().toJson();
	}

}
