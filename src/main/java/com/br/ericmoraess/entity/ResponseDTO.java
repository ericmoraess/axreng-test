package com.br.ericmoraess.entity;

import java.util.List;

import com.br.ericmoraess.common.RandomNumberBuilder;

public class ResponseDTO extends DTOJson implements IResult  {
	private String id;
	private String status;
	private List<String> urls;
	
	public ResponseDTO(String status, List<String> urls) {
		this.id = RandomNumberBuilder.getInstance().setUseLetters(true).setUseNumbers(true).build().getId();
		this.status = status;
		this.urls = urls;
	}

	@Override
	public String getId() {
		return id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<String> getURLs() {
		return urls;
	}
}
