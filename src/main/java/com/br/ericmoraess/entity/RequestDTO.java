package com.br.ericmoraess.entity;

public class RequestDTO extends DTOJson {
	private String keyword;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
}
