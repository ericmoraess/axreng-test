package com.br.ericmoraess.entity;

import java.util.List;

public interface IResult {
	public String getId(); 
	public String getStatus();
	public void setStatus(String status);
	public List<String> getURLs();
	public String toJson();
}
