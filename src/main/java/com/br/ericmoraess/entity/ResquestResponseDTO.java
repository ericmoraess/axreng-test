package com.br.ericmoraess.entity;

public class ResquestResponseDTO extends DTOJson {
	private String id;
	
	public ResquestResponseDTO() {
		
	}
	
	public ResquestResponseDTO(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
