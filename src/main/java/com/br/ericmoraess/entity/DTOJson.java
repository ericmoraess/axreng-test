package com.br.ericmoraess.entity;

import com.google.gson.Gson;

public abstract class DTOJson {
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	public Object parser(String json, Class clazz) {
		Gson gson = new Gson();
		return gson.fromJson(json, clazz);
	}
}
