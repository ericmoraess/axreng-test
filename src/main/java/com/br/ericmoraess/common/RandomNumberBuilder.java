package com.br.ericmoraess.common;

public final class RandomNumberBuilder {
	private static RandomNumberBuilder self;
	private boolean useLetters;
    private boolean useNumbers;
	
	private RandomNumberBuilder() {}
	
	public static RandomNumberBuilder getInstance() {
		if(self == null) {
			self = new RandomNumberBuilder();
		}
		return self;
	}
	
	public RandomNumberBuilder setUseLetters(boolean isUseLetters) {
		this.useLetters = isUseLetters;
		return this;
	}
	
	public RandomNumberBuilder setUseNumbers(boolean isUseNumbers) {
		this.useNumbers = isUseNumbers;
		return this;
	}
	
	public RandomNumber build() {
		return new RandomNumber(this.useLetters, this.useNumbers);
	}
}
