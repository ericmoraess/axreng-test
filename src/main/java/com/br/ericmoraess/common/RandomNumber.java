package com.br.ericmoraess.common;

import org.apache.commons.lang3.RandomStringUtils;

public final class RandomNumber {
	private static final int DEFAULT_LENGHT_NUMBER = 8; 
	private final String id;
	private boolean useLetters;
	private boolean useNumbers;

	public RandomNumber(boolean useLetters, boolean useNumbers) {
		this.id = RandomStringUtils.random(DEFAULT_LENGHT_NUMBER, useLetters, useNumbers);
		this.useLetters = useLetters;
		this.useNumbers = useNumbers;
	}

	public String getId() {
		return id;
	}

	public boolean isUseLetters() {
		return useLetters;
	}

	public boolean isUseNumbers() {
		return useNumbers;
	}
}
